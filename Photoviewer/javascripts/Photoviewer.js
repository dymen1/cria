/* jshint loopfunc: true, browser: true */ // reason loopfunc:true = The reason for the warning is to avoid the bug frequently encountered if you were using i in an asynchronous callback (onclick or ajax).
(function () {
    var lastSelected;
    var shrinkFactor = 0.7;
    var images = [
        ["0004681", "0004713", "0004720", "0004731", "0004750",
            "0004755", "0004801", "0004802", "0004827", "0004853",
            "0004858", "0004860", "0004870", "0004874", "0004888",
            "0004902", "0004931", "0004969", "0004971", "0006598",
            "0006630", "0006638", "0006680", "0006743", "0006787"],
        ["Jan", "Jaap", "WIllem", "Kees", "Piet",
            "Jan", "Jaap", "WIllem", "Kees", "Piet",
            "Jan", "Jaap", "WIllem", "Kees", "Piet",
            "Jan", "Jaap", "WIllem", "Kees", "Piet",
            "Jan", "Jaap", "WIllem", "Kees", "Piet"],
        ["Arnhem", "Nijmegen", "Op zee", "Thuis", "Nederland",
            "Arnhem", "Nijmegen", "Op zee", "Thuis", "Nederland",
            "Arnhem", "Nijmegen", "Op zee", "Thuis", "Nederland",
            "Arnhem", "Nijmegen", "Op zee", "Thuis", "Nederland",
            "Arnhem", "Nijmegen", "Op zee", "Thuis", "Nederland"]
    ];
    window.crud = {

        createNodes: function () {
            var crud = window.crud;
            var bodyNode, articleNode, imageNode, commentNode;

            // determines gallery size
            crud.setBodySize(0, false);

            // determines the grid-layout size
            crud.setArticleDimensions(1, false, undefined);

            // Get body node
            bodyNode = document.getElementsByTagName("body")[0];

            // Create a new article elements
            for (var i = 0; i < images[0].length; i++) {

                // Create a new article elements
                articleNode = document.createElement("article");
                bodyNode.appendChild(articleNode);

                // Create a new image elements
                imageNode = document.createElement("img");
                imageNode.src = "images/" + images[0][i] + ".jpg";
                imageNode.alt = "";

                // adds slight tilt to images
                var degrees = [5, 3, 0 , -3, -5];
                var degree = (Math.floor(Math.random() * 5));
                imageNode.style.cssText = "transform:rotate(" + (degrees[degree]) + "deg);" +
                    " -ms-transform:rotate(" + (degrees[degree]) + "deg); /* IE 9 */ " +
                    "-webkit-transform:rotate(" + (degrees[degree]) + "deg); /* Safari and Chrome */";

                // changes te dimensions of an image according to it's original dimensions,
                // can't use the setDimension because giving this as parameter won't give the same result as using ot directly
                imageNode.onload = function () {
                    var hoogte = screen.height / Math.ceil(Math.sqrt(images[0].length)) * shrinkFactor;
                    if (this.naturalHeight < this.naturalWidth) {
                        this.style.width = (screen.width / 2) / Math.ceil(Math.sqrt(images[0].length)) * shrinkFactor + "px";
                        this.style.height = "auto";
                        this.style.position = "absolute";
                        this.style.top = "70%";
                        this.style.marginTop = hoogte / 2 * -1 + "px";
                    } else {
                        this.style.height = hoogte + "px";
                        this.style.width = "auto";
                    }
                };

                // Append new nodes to article node
                articleNode.appendChild(imageNode);
            }

            // Comment node code
            commentNode = document.createComment("Comment in HTML");
            bodyNode.appendChild(commentNode);
        },

        retrieveNodes: function () {

            /**
             * Returns an object reference to the identified element.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.getElementById
             */
            document.getElementById("someId");

            /**
             * Returns a list of elements with the given class name.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.getElementsByClassName
             */
            document.getElementsByClassName("someClass");

            /**
             * Returns a list of elements with the given tag name and namespace.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.getElementsByTagName
             */
            document.getElementsByTagName("body");

            /**
             * Returns a list of elements with the given name.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.getElementsByName
             */
            document.getElementsByName("someName");

            /**
             * Returns the first Element node within the document, in document order, that matches the specified selectors.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.querySelector
             */
            document.querySelector("div.someClass p.someOtherClass");

            /**
             * Returns a list of all the Element nodes within the document that match the specified selectors.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.querySelectorAll
             */
            document.querySelectorAll("div.someClass p.someOtherClass");

        },

        updateNodes: function (event) {
            // makes it impossible to zoom when there's 1 or less images.
            if (images[0].length <= 1) {
                return;
            }

            var crud = window.crud;
            // Var used to keep track of the element that the zoom will focus on.
            var elementMouseIsOver;

            // If-statement that checks if space-bar has been pressed,
            // if no image has previously been selected --> zoom on middle image of the array.
            // If the space-bar has been pressed --> zoom on previously zoomed image.
            // If none if the above is true --> make the "screen" zoom out.
            if (event.keyCode === 32 && lastSelected === undefined) {
                var imageElements = document.getElementsByTagName("img");
                elementMouseIsOver = imageElements[Math.ceil(imageElements.length / 2)];
            } else if (event.keyCode === 32 && lastSelected !== undefined) {
                if (document.getElementsByClassName("center").length === 0) {
                    elementMouseIsOver = lastSelected;
                } else {
                    lastSelected.parentNode.classList.remove("center");
                }
            }

            // code for arrow-key movement
            if (event.keyCode >= 37 && event.keyCode <= 40) {
                var moved = false;
                if (document.getElementsByClassName("center").length !== 0) {
                    var elements = document.getElementsByTagName("article");
                    for (var i = 0; i < elements.length; i++) {
                        if (elements[i].outerHTML === lastSelected.parentNode.outerHTML && moved === false) {
                            var newZoomedEle;
                            if (event.keyCode === 39 && i < elements.length) {
                                newZoomedEle = i + 1;
                            } else if (event.keyCode === 37 && i > 0) {
                                newZoomedEle = i - 1;
                            } else if (event.keyCode === 38 && (i - Math.ceil(Math.sqrt(images[0].length))) > 0) {
                                newZoomedEle = i - Math.ceil(Math.sqrt(images[0].length));
                            } else if (event.keyCode === 40 && (i + Math.ceil(Math.sqrt(images[0].length))) < elements.length) {
                                newZoomedEle = i + Math.ceil(Math.sqrt(images[0].length));
                            }
                            if (newZoomedEle !== undefined) {
                                elements[newZoomedEle].classList.add("center");
                                elementMouseIsOver = elements[newZoomedEle].children[0];
                                document.body.removeChild(document.getElementsByClassName("overlay")[0]);
                                lastSelected.parentNode.classList.remove("center");
                                moved = true;
                            } else {
                                return;
                            }
                        }
                    }
                }
            }

            // Checks if there is a element to zoom to, if not then it will get the element under the cursor.
            if (elementMouseIsOver === undefined) {
                var x = event.clientX, y = event.clientY;
                elementMouseIsOver = document.elementFromPoint(x, y);
            }

            // Checks if the element is a image element
            // sets the "global" lastSelected var and sets a class to the element that will be zoomed to.
            // If something else than a image element is selected the screen will zoom out.
            if (elementMouseIsOver.outerHTML.indexOf("img") === 1 && elementMouseIsOver.outerHTML.indexOf("article") === -1) {
                if (elementMouseIsOver === lastSelected && elementMouseIsOver.parentNode.classList.contains("center")) {
                    crud.zoom(false);
                    lastSelected.parentNode.classList.remove("center");
                } else {
                    if (lastSelected !== undefined) {
                        lastSelected.parentNode.classList.remove("center");
                    }
                    crud.zoom(true, elementMouseIsOver);
                    elementMouseIsOver.parentNode.classList.add("center");
                    lastSelected = elementMouseIsOver;
                }
            } else if (lastSelected !== undefined) {
                crud.zoom(false);
                lastSelected.parentNode.classList.remove("center");
            }
        },

        deleteNodes: function (event) {
            var crud = window.crud;
            var x = event.clientX, y = event.clientY, elementMouseIsOver = document.elementFromPoint(x, y);
            if (elementMouseIsOver.outerHTML.indexOf("img") !== -1 && elementMouseIsOver.outerHTML.indexOf("article") === -1 && elementMouseIsOver.parentNode.outerHTML.indexOf("section") === -1) {
                var elements = document.getElementsByTagName("article");
                // Deletes DOM element from parent and the src from the images array,
                // the images array is used to determine the grid-layout size
                for (var i = 0; i < elements.length; i++) {
                    if (elements[i].children[0].outerHTML === elementMouseIsOver.parentNode.children[0].outerHTML) {
                        for (var j = 0; j < images[0].length; j++) {
                            if (elements[i].children[0].src.indexOf(images[0][j]) !== -1) {
                                images[0].splice(j, 1);
                            }
                        }
                        var who = elements[i];
                        who.parentNode.removeChild(who);
                    }
                }
                // recalculate the grid-layout
                crud.zoom(false);
                crud.setImageDimensions(true);
                crud.setArticleDimensions(1, false);
                return false;
            }
            return true;
        },

        zoom: function (value, elementMouseIsOver) {
            if (value && document.getElementsByClassName("overlay").length === 0) {
                var overlay = document.createElement("section");
                overlay.setAttribute("class", "overlay");
                document.body.appendChild(overlay);

                var containerNode = document.createElement("section");
                overlay.appendChild(containerNode);

                var zoomedNode = elementMouseIsOver.cloneNode(true);
                if (zoomedNode.naturalHeight < zoomedNode.naturalWidth) {
                    zoomedNode.style.width = (screen.width / 2) + "px";
                    zoomedNode.style.height = "auto";
                    zoomedNode.style.marginTop = screen.height / 2 * -1 + "px";
                } else {
                    zoomedNode.style.height = screen.height * 0.7 + "px";
                    zoomedNode.style.width = "auto";
                    zoomedNode.style.marginTop = "1em";
                }
                zoomedNode.style.border = "1em solid white";
                containerNode.appendChild(zoomedNode);

                var details = document.createElement("p");
                containerNode.appendChild(details);

                var detailAuthor = document.createTextNode("Taken by: " + images[1][1]);
                details.appendChild(detailAuthor);

                var brNode = document.createElement("br");
                details.appendChild(brNode);

                var detailLocation = document.createTextNode("Location: " + images[2][1]);
                details.appendChild(detailLocation);

            } else if (document.getElementsByClassName("overlay").length === 1) {
                document.body.removeChild(document.getElementsByClassName("overlay")[0]);
            }
        },

        setImageDimensions: function (useGrid) {
            var crud = window.crud;
            var articleElements = document.getElementsByTagName("article");
            for (var j = 0; j < articleElements.length; j++) {
                crud.setImageDimension(articleElements[j].children[0], useGrid);
            }
        },

        setImageDimension: function (imageElement, useGrid) {
            var gridFactor = 1;

            imageElement.style.border = "20px solid white";
            if (useGrid) {
                gridFactor = Math.ceil(Math.sqrt(images[0].length));
                imageElement.style.border = "3px solid white";
            }
            var hoogte = screen.height / gridFactor;

            if (imageElement.naturalHeight < imageElement.naturalWidth) {
                imageElement.style.width = (screen.width / 2) / gridFactor * shrinkFactor + "px";
                imageElement.style.height = "auto";
                imageElement.style.top = gridFactor * 5 + 50 + "%";
                imageElement.style.marginTop = hoogte / 2 * -1 + "px";
            } else {
                imageElement.style.height = hoogte * shrinkFactor + "px";
                imageElement.style.width = "auto";
            }
        },

        // TODO maybe add stylesheetSlot remove useZoom
        setArticleDimensions: function (articleRuleSlot, useZoom, article) {
            var gridFactor = 1;

            if (!useZoom) {
                gridFactor = Math.ceil(Math.sqrt(images[0].length));
            }

            if (article === undefined) {
                document.styleSheets[0].cssRules[articleRuleSlot].style.width = (screen.width / 2) / gridFactor * shrinkFactor + "px";
                document.styleSheets[0].cssRules[articleRuleSlot].style.height = screen.height / gridFactor * shrinkFactor + "px";
            } else {
                article.style.width = (screen.width / 2) / gridFactor + "px";
                article.style.height = screen.height / gridFactor + "px";
            }
        },

        // TODO maybe add stylesheetSlot
        setBodySize: function (bodyRuleSlot, zoomed) {
            var gridFactor = 1;

            document.styleSheets[0].cssRules[bodyRuleSlot].style.marginTop = "2em";
            if (zoomed) {
                gridFactor = Math.ceil(Math.sqrt(images[0].length));
                document.styleSheets[0].cssRules[bodyRuleSlot].style.marginTop = "0em";
            }
            document.styleSheets[0].cssRules[bodyRuleSlot].style.width = screen.width / 2 * 1.12 * gridFactor * shrinkFactor + "px";
        },

        /**
         *  Init function that creates the page content and retrieveNode functions.
         */
        init: function () {
            var crud = window.crud;
            crud.createNodes();
            crud.retrieveNodes();
            //crud.updateNodes();
            //crud.deleteNodes();
        }
    };
}() );

/**
 * On-load function that calls the init.
 */
window.onload = function () {
    var crud = window.crud;
    crud.init();
};

/**
 * Onclick and space-bar event functions.
 */
document.onclick = function (e) {
    var crud = window.crud;
    if (1 === e.which || 1 === e.button) {
        crud.updateNodes(e);
    }
};

// TODO add default event for firefox
document.onkeydown = function (e) {
    var crud = window.crud;
    if (e.keyCode === 32 || e.keyCode >= 37 && e.keyCode <= 40) { // space-bar and arrow keys
        crud.updateNodes(e);
    }
};

window.oncontextmenu = function (e) {
    var crud = window.crud;
    return crud.deleteNodes(e);
//    return false;
};