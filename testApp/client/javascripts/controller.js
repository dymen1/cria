/*jslint node: true, plusplus: true, todo: true  */
/*globals  */

"use strict";


/*REQUEST*/

function RequestListCtrl($scope, requestService) {
    $scope.request = requestService.request.get();
}

function RequestDetailCtrl($scope, $routeParams, requestService, $location) {
    console.log("test", $routeParams._id);
    $scope.request = requestService.request.get({_id: $routeParams._id}, function(){
        console.log('$scope.request ', $scope.request);
    });
    // DELETE request
    $scope.delete = function(){
        console.log('entering delete');
        requestService.request.delete({_id: $routeParams._id});
        $location.path("/request/");
    }
    // CREATE, UPDATE request
    $scope.save = function () {
        if($scope.request.doc && $scope.request.doc._id !== undefined){
            console.log('entering update');
            requestService.request.update({_id: $scope.request.doc._id}, $scope.request.doc, function(res){
                console.log(res);
            });
        } else {
            console.log('entering save');
            requestService.request.save({}, $scope.request.doc, function(res){
                console.log(res);
            });
        }
    }
}

/*jslint node: true, plusplus: true, todo: true  */
/*globals  */

/*USER*/
function UserListCtrl($scope, userService) {
    $scope.user = userService.user.get();
}

function UserDetailCtrl($scope, $routeParams, userService, $location) {
    $scope.user = userService.user.get({_id: $routeParams._id}, function(){
        console.log('$scope.user ', $scope.user);
    });
    // DELETE user
    $scope.delete = function(){
        console.log('entering delete');
        userService.user.delete({_id: $routeParams._id});
        $location.path("/user/");
    }
    // CREATE, UPDATE user
    $scope.save = function () {
        if($scope.user.doc && $scope.user.doc._id !== undefined){
            console.log('entering update');
            console.log($scope.user);
            $scope.user.doc.groups = $scope.user.groups;

            //$scope.user.group=null;
            userService.user.update({_id: $scope.user.doc._id}, $scope.user.doc, function(res){

                console.log($scope.user);
            });
        } else {
            console.log('entering save');
            var pwfield = document.getElementById("PWfield");
       //     pwfield.style="data-ng-model=user.doc.password";
            userService.user.save({}, $scope.user.doc, function(res){
                console.log(res);
            });
        }
    }
}

/*Groups*/
function GroupListCtrl($scope, groupService) {
    $scope.group = groupService.group.get();
}

function GroupDetailCtrl($scope, $routeParams, groupService, $location) {
    console.log("test", $routeParams._id);
    $scope.group = groupService.group.get({_id: $routeParams._id}, function(){
        console.log('$scope.group ', $scope.group);
    });
    // DELETE group
    $scope.delete = function(){
        console.log('entering delete');
        groupService.group.delete({_id: $routeParams._id});
        $location.path("/group/");
    }
    // CREATE, UPDATE group
    $scope.save = function () {
        if($scope.group.doc && $scope.group.doc._id !== undefined){
            console.log('entering update');
            groupService.group.update({_id: $scope.group.doc._id}, $scope.group.doc, function(res){
                console.log(res);
            });
        } else {
            console.log('entering save');
            groupService.group.save({}, $scope.group.doc, function(res){
                console.log(res);
            });
        }
    }
}

/*Tests*/
function TestListCtrl($scope, testService) {
    $scope.test = testService.test.get();
}

function TestDetailCtrl($scope, $routeParams, testService, $location) {
    console.log("test", $routeParams._id);
    $scope.test = testService.test.get({_id: $routeParams._id}, function(){
        console.log('$scope.test ', $scope.test);
    });
    // DELETE Tests
    $scope.delete = function(){
        console.log('entering delete');
        testService.test.delete({_id: $routeParams._id});
        $location.path("/test/");
    }
    // CREATE, UPDATE Tests
    $scope.save = function () {
        if($scope.test.doc && $scope.test.doc._id !== undefined){
            console.log('entering update');
            testService.test.update({_id: $scope.test.doc._id}, $scope.test.doc, function(res){
                console.log(res);
            });
        } else {
            console.log('entering save');
            testService.test.save({}, $scope.test.doc, function(res){
                console.log(res);
            });
            console.log('finish save');
        }
    }
}

/*Scenario*/
function ScenarioListCtrl($scope, scenarioService) {
    $scope.scenario = scenarioService.scenario.get();
}

function ScenarioDetailCtrl($scope, $routeParams, scenarioService, $location, $http) {
    console.log("test", $routeParams._id);
    $scope.scenario = scenarioService.scenario.get({_id: $routeParams._id}, function(){
        console.log('$scope.scenario ', $scope.scenario);
    });
    // DELETE Scenario
    $scope.delete = function(){
        console.log('entering delete');
        scenarioService.scenario.delete({_id: $routeParams._id});
        $location.path("/scenario/");
    }

    $scope.executeScenario = function(){
        console.log('entering execute');
     //   console.log($routeParams);
        $http.get("/executeScenario/" + $routeParams._id).success(function() {
            console.log("get succes");
        });
    }


    // CREATE, UPDATE Scenario
    $scope.save = function () {
        if($scope.scenario.doc && $scope.scenario.doc._id !== undefined){
            console.log('entering update');
            scenarioService.scenario.update({_id: $scope.scenario.doc._id}, $scope.scenario.doc, function(res){
                console.log(res);
            });
        } else {
            console.log('entering save');
            scenarioService.scenario.save({}, $scope.scenario.doc, function(res){
                console.log(res);
            });
            console.log('finish save');
        }
    }
}