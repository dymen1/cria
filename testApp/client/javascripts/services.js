"use strict";


angular.module('myApp.services', ['ngResource'])
    .factory('requestService', ['$resource', '$http',
        function($resource){
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'delete': {method: 'DELETE'}
                },
                db = {};
            db.request = $resource('/request/:_id', {}, actions);
            return db;
        }

    ])
    .factory('userService', ['$resource', '$http',
        function($resource){
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'delete': {method: 'DELETE'}
                },
                db = {};
            db.user = $resource('/user/:_id', {}, actions);
            return db;
        }

    ])
    .factory('groupService', ['$resource', '$http',
        function($resource){
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'delete': {method: 'DELETE'}
                },
                db = {};
            db.group = $resource('/group/:_id', {}, actions);
            return db;
        }

    ])
    .factory('testService', ['$resource', '$http',
        function($resource){
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'delete': {method: 'DELETE'}
                },
                db = {};
            db.test = $resource('/test/:_id', {}, actions);
            return db;
        }

    ])
    .factory('scenarioService', ['$resource', '$http',
        function($resource){
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'delete': {method: 'DELETE'}
                },
                db = {};
            db.scenario = $resource('/scenario/:_id', {}, actions);
            return db;
        }

    ])
;

