
"use strict";

/**
 * @see http://docs.angularjs.org/guide/concepts
 */
angular.module('myApp', [ 'myApp.services'])
    .config(['$routeProvider', function ($routeProvider) {


        // Get all requests
        $routeProvider.when('/request', {
            templateUrl: 'partials/request-list.html',
            controller: RequestListCtrl
        });

        // Get 1 request
        $routeProvider.when('/request/:_id', {
            templateUrl: 'partials/request-detail.html',
            controller: RequestDetailCtrl
        });

        // Get all requests
        $routeProvider.when('/user', {
            templateUrl: 'partials/user-list.html',
            controller: UserListCtrl
        });

        // Get 1 request
        $routeProvider.when('/user/:_id', {
            templateUrl: 'partials/user-detail.html',
            controller: UserDetailCtrl
        });

        // Get all requests
        $routeProvider.when('/group', {
            templateUrl: 'partials/group-list.html',
            controller: GroupListCtrl
        });

        // Get 1 request
        $routeProvider.when('/group/:_id', {
            templateUrl: 'partials/group-detail.html',
            controller: GroupDetailCtrl
        });

        // Get all tests
        $routeProvider.when('/test', {
            templateUrl: 'partials/test-list.html',
            controller: TestListCtrl
        });

        // Get 1 test
        $routeProvider.when('/test/:_id', {
            templateUrl: 'partials/test-detail.html',
            controller: TestDetailCtrl
        });

        // Get all scenarios
        $routeProvider.when('/scenario', {
            templateUrl: 'partials/scenario-list.html',
            controller: ScenarioListCtrl
        });

        // Get 1 scenario
        $routeProvider.when('/scenario/:_id', {
            templateUrl: 'partials/scenario-detail.html',
            controller: ScenarioDetailCtrl
        });


        // When no valid route is provided
        $routeProvider.otherwise({
            redirectTo: "/#"
        });

    }]);