/**
 * Created by EDDY on today.
 */

var mongoose = require('mongoose')
    , Request = mongoose.model('Request');


// CREATE
exports.create = function (req, res) {

    console.log('create Request');

    var doc = new Request(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
}

// RETRIEVE
exports.list = function (req, res) {
    var conditions, fields, options;

    console.log('get all Requests.');

    conditions = {},
        fields = {},
        sort = {'modificationDate': -1};

    Request
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}


exports.detail = function (req, res) {
    var conditions, fields, options;

    console.log('get all Requests');

    conditions = {_id: req.params._id},
        fields = {},
        sort = {'createdAt': -1};

    Request
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        })
}

// UPDATE
exports.update = function (req, res) {
    var conditions, update, options;

    conditions = {_id: req.params._id},
        update = {
            name: req.body.name || '',
            hostname: req.body.hostname || '',
            port: req.body.port || '',
            path: req.body.path || '',
            method: req.body.method || '',
            description: req.body.description || ''
        },
        options = {multi: false},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        };

    Request.findOneAndUpdate(conditions, update, options, callback);
}


// DELETE
exports.delete = function (req, res) {
    var conditions, update, options;

    console.log('deleting Request...\n ', req.params._id);


    conditions = {_id: req.params._id},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        };

    Request.remove(conditions, callback);
}