/**
 * Created by EDDY on today.
 */

var mongoose = require('mongoose')
    , Group = mongoose.model('Group')
    , User = mongoose.model('User')
    , passwordHash = require('password-hash');


// CREATE
exports.create = function (req, res) {

    console.log('create User');

    req.body.password = passwordHash.generate(req.body.password || "topSecret!");

    var doc = new User(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
}

// RETRIEVE
exports.list = function (req, res) {
    var conditions, fields, options;

    console.log('get all Users.');

    conditions = {},
        fields = {},
        sort = {'modificationDate': -1};

    User
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}


exports.detail = function (req, res) {
    var conditions, fields, options, retDoc, i, j, groupDoc;

    console.log('GET 1 user. ' + req.params._id);

    conditions = req.params._id
        , fields = {}
        , options = {'createdAt': -1};

    User
        .findById(conditions, fields, options)
        .populate("Group")// <------------------ populating sub documents, only for demo, not needed here
        .exec(function (err, doc) {

            var groups = [];
            // Create a array, so that we can compare all groups with existing groups
            if (!doc) {
                doc = {};
                doc.gender = "male";
                doc.picture = "user.png";
            } else if (doc && doc.groups) {
                for (i = 0; i < doc.groups.length; i++) {
                    groups.push(doc.groups[i]._id + "");
                }
            }

            // Find groups for user
            Group
                .find({}, function (err, groupsDoc) {
                    retDoc = [];
                    for (i = 0; i < groupsDoc.length; i++) {
                        groupDoc = {
                            _id: groupsDoc[i]._id,
                            name: groupsDoc[i].name,
                            isMember: false
                        };

                        if (groups.indexOf(groupsDoc[i]._id + "") >= 0) {
                            groupDoc.isMember = true;
                        }
                        retDoc.push(groupDoc);
                    }

                    var retObj = {
                        meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                        doc: doc,
                        groups: retDoc,
                        err: err
                    };
                    return res.send(retObj);
                });
        })
}

function updateGroupsWithUser(err, req, res, groups, doc) {
    var group;
    if (!groups || groups.length === 0) {
        // Return if no array or empty array (consider alternative $pullAll)
        var retObj = {
            meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    } else {
        // Get first element from groups array.
        group = groups.pop();
        // Check if group has to be added or excluded from groups based on attribute "isMember"
        if (group.isMember) {
            // Add to set
            User
                .update({_id: doc._id}, {$addToSet: {"groups": group}}, function (res1) {
                    updateGroupsWithUser(err, req, res, groups, doc);
                });
        } else {
            // Remove from set
            User
                .update({_id: doc._id}, {$pull: {"groups": group}}, function (res1) {
                    updateGroupsWithUser(err, req, res, groups, doc);
                });
        }
    }
}
// UPDATE
exports.update = function (req, res) {
    var conditions, update, options;

    console.log('updating User...\n ', req.params._id, req.user);

    conditions = {_id: req.params._id},
        update = {
            name: req.body.name || '',
            sex: req.body.sex || '',
            sex: req.body.password || '',
            age: req.body.age || ''
        },
        options = {multi: false},
        callback = function (err, doc) {
            updateGroupsWithUser(err, req, res, req.body.groups, doc);
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        };
    if (req.body.doc.password && req.body.doc.password !== '') {
        // Make sure that password is hashed.
        update.password = passwordHash.generate(req.body.doc.password || "topSecret!");
    }

    User.findOneAndUpdate(conditions, update, options, callback);
}


// DELETE
exports.delete = function (req, res) {
    var conditions, update, options;

    console.log('deleting User...\n ', req.params._id);


    conditions = {_id: req.params._id},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        };

    User.remove(conditions, callback);
}/**
 * Created by User on 2-4-14.
 */
