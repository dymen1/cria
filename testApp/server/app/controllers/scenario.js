/**
 * Created by EDDY on today.
 */
var mongoose = require('mongoose')
    , Scenario = mongoose.model('Scenario')
    , Request = mongoose.model('Request')
    , Test = mongoose.model('Test')
    , http = require('http');


// CREATE
exports.create = function (req, res) {

    console.log('create Scenario');

    var doc = new Scenario(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
}

// RETRIEVE
exports.list = function (req, res) {
    var conditions, fields, options;

    console.log('get all Scenarios.');

    conditions = {},
        fields = {},
        sort = {'modificationDate': -1};

    Scenario
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}


exports.detail = function (req, res) {
    var conditions, fields, options;

    console.log('get all Scenarios');

    conditions = {_id: req.params._id},
        fields = {},
        sort = {'createdAt': -1};

    Scenario
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        })
}

// UPDATE
exports.update = function (req, res) {
    var conditions, update, options;

    conditions = {_id: req.params._id},
        update = {
            name: req.body.name || '',
            sequence: req.body.sequence || '',
            testSet:{
                requestID: req.body.testSet.requestID || '',
                testID: req.body.testSet.testID || ''
             }
        },
        options = {multi: false},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        };

    Scenario.findOneAndUpdate(conditions, update, options, callback);
}


// DELETE
exports.delete = function (req, res) {
    var conditions, update, options;

    console.log('deleting Scenario...\n ', req.params._id);


    conditions = {_id: req.params._id},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        };

    Scenario.remove(conditions, callback);
}


exports.executeScenario = function (req, res) {
    var conditions, fields, options, scenario, request;
    console.log("execute scenario \n");

       getScenario(req, res);
}

function getScenario(req, res) {
    var conditions, update, options, scenario;
    conditions = {_id: req.params._id},
        fields = {},
        sort = {'createdAt': -1};

    Scenario
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            scenario = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            getRequest(scenario, res);
        })
}

function getRequest(scenario, res) {
    var conditions, options, request;

    conditions = {_id: scenario.doc.testSet.requestID},
        fields = {},
        sort = {'createdAt': -1};

    Request
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var request = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            getTest(scenario, res, request);
        })
}


function getTest(scenario, res, request) {
    var conditions, options, test;

    conditions = {_id: scenario.doc.testSet.testID},
        fields = {},
        sort = {'createdAt': -1};

    Test
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var test = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            doRequest(request, scenario, test, res);
        })
}


function doRequest(request, scenario, test, res1) {
    var data, options, expression, operator, expectation;
    expression = "data.query." + test.doc.expression;
    operator = test.doc.operator;
    expectation = test.doc.expectation;
    console.log(test.doc)
        options = {
        hostname: request.doc.hostname,
        port: parseFloat(request.doc.port),
        path: request.doc.path,
        method: request.doc.method
    };
    var req = http.request(options, function(res, test) {
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        data = '';
        res.on('data', function (chunk) {
            data += chunk;
        });
        res.on('end', function () {
           data = JSON.parse(data);

            res1.send(data);

           if(operator == "==="){
               if(eval(expression) === eval(expectation)){
                   console.log("TEST + JEEEJ");
               } else{
                   console.log("TEST=== + NOOO");
               }
           } else if(operator == "!=="){
               if(eval(expression) !== eval(expectation)){
                   console.log("TEST + JEEEJ");
               }  else{
                   console.log("TEST !== + NOOO");
               }
           }
        });
    });

    req.on('error', function(e) {
        console.log('problem with request: ' + e.message)
    });
    // write data to request body
    req.write('data\n');
    req.write('data\n');
    req.end();
}


