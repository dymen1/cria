/**
* Module dependencies.
*/
var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
* schema definition
*/
var schemaName = Schema({
    name: {type: String, required: true},
    expression: {type: String, required: true},
    operator: {type: String, required: true},
    expectation: {type: String, required: true},
    description: {type: String, "default": ""},
    modificationDate: {type: Date, "default": Date.now}
});


var modelName = "Test";
var collectionName = "tests"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

