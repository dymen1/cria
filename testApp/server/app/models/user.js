/**
* Module dependencies.
*/
var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var groupSchema = Schema({
    _id: {type: Schema.Types.ObjectId, ref: "Group"}
});
/**
* schema definition
*/
var schemaName = Schema({
    name: {type: String, required: true},
    sex: {type: String, required: true},
    age: {type: String},
    password:{type: String, required: true},
    modificationDate: {type: Date, "default": Date.now},
    groups: [groupSchema], // <----- sub document
    meta: {} // anything goes
});


var modelName = "User";
var collectionName = "users"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

