/**
 * Module dependencies.
 */
var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * schema definition
 */
var schemaName = Schema({
    name: {type: String, required: true},
    sequence: {type: String},
    testSet: { // <--------------- nested document
        testID: {type: String, required: true},
        requestID: {type: String, required: true}
    },
    modificationDate: {type: Date, "default": Date.now}
});


var modelName = "Scenario";
var collectionName = "Scenarios"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

