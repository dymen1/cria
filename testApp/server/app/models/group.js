/**
 * Module dependencies.
 */
var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var userSchema = Schema({
    _id: {type: Schema.Types.ObjectId, ref: "User"}
});
/**
 * schema definition
 */
var schemaName = Schema({
    name: {type: String, required: true},
    description: {type: String, required: true},
    users: [userSchema], // <------------------------ sub document
    meta: {}, // anything goes
    modificationDate: {type: Date, "default": Date.now}
});


var modelName = "Group";
var collectionName = "groups"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

