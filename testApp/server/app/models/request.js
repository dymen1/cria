/**
 * Module dependencies.
 */
var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * schema definition
 */
var schemaName = Schema({
    name: {type: String, required: true},
    hostname: {type: String, required: true},
    port: {type: String, required: true, "default": 3000},
    path: {type: String, required: true},
    method: {type: String, required: true},
    parameters: {name:{type: String}, value: {type: String}},
    description: {type: String, "default": ""},
    modificationDate: {type: Date, "default": Date.now}
});


var modelName = "Request";
var collectionName = "requests"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

