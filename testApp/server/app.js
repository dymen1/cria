/**
 * Created by theotheu on 27-10-13.
 */

/**
 * Module dependencies.
 */
var express = require('express')
    , fs = require('fs')
    , http = require('http')
    , path = require('path')
    ;

// Load configuration
var env = process.env.NODE_ENV || 'development'
    , config = require('./config/config.js')[env];

// Bootstrap db connection
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
mongoose.connect(config.db);

// Bootstrap models
var models_path = __dirname + '/app/models'
    , model_files = fs.readdirSync(models_path);
model_files.forEach(function (file) {
    require(models_path + '/' + file);
})

var app = express();
var env = process.env.NODE_ENV || 'development';
if ('development' == env) {
    app.set('port', process.env.PORT || config.port);
    // FIXME: Not used, also remove ejs from package.json
    app.set('view engine', 'ejs');
    // FIXME: Not used, also remove ejs from package.json
    app.set('views', path.join(__dirname, './app/views/'));
    // FIXME: Not used, also remove ejs from package.json
//    app.use(express.favicon());
//    app.use(express.logger('dev'));
//    app.use(express.json());
//    app.use(express.urlencoded());
//    app.use(express.methodOverride());
//    app.use(express.cookieParser('your secret here'));
//    app.use(express.session());
//    app.use(app.router);
    // FIXME: Not used, also remove styles from package.json
//    app.use(require('stylus').middleware(__dirname + '../client'));
    app.use(express.static(path.join(__dirname, '../client')));
//    app.use(express.errorHandler());
}

// Bootstrap http server
http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port " + app.get('port'));
});

// Bootstrap routes
var routes_path = __dirname + '/routes'
    , route_files = fs.readdirSync(routes_path);
route_files.forEach(function (file) {
    require(routes_path + '/' + file)(app);
})

// Last line to serve static page
console.log('last resort');
app.use(express.static(__dirname + '../client/'));
