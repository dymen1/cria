
module.exports = function (app) {
    /*  request routes
     ---------------
     We create a variable "user" that holds the controllers object.
     We map the URL to a method in the created variable "user".
     In this example is a mapping for every CRUD action.
     */
    var controller = require('../app/controllers/user.js');

    // CREATE
    app.post('user', controller.create);
    // RETRIEVE
    app.get('user', controller.list);
    app.get('user/:_id', controller.detail);

    // UPDATE
    app.put('user/:_id', controller.update);

    // DELETE
    app.delete('user/:_id', controller.delete);

}
