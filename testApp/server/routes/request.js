
module.exports = function (app) {
    /*  request routes
     ---------------
     We create a variable "user" that holds the controllers object.
     We map the URL to a method in the created variable "user".
     In this example is a mapping for every CRUD action.
     */
    var controller = require('../app/controllers/request.js');

    // CREATE
    app.post('request', controller.create);
    // RETRIEVE
    app.get('request', controller.list);
    app.get('request/:_id', controller.detail);

    // UPDATE
    app.put('request/:_id', controller.update);

    // DELETE
    app.delete('request/:_id', controller.delete);

}
