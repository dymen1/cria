
module.exports = function (app) {
    /*  request routes
     ---------------
     We create a variable "user" that holds the controllers object.
     We map the URL to a method in the created variable "user".
     In this example is a mapping for every CRUD action.
     */
    var controller = require('../app/controllers/test.js');

    // CREATE
    app.post('test', controller.create);
    // RETRIEVE
    app.get('test', controller.list);
    app.get('test/:_id', controller.detail);

    // UPDATE
    app.put('test/:_id', controller.update);

    // DELETE
    app.delete('test/:_id', controller.delete);

}
