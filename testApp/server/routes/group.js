
module.exports = function (app) {
    /*  request routes
     ---------------
     We create a variable "user" that holds the controllers object.
     We map the URL to a method in the created variable "user".
     In this example is a mapping for every CRUD action.
     */
    var controller = require('../app/controllers/group.js');

    // CREATE
    app.post('group', controller.create);
   // RETRIEVE
    app.get('group', controller.list);
    app.get('group/:_id', controller.detail);

    // UPDATE
    app.put('group/:_id', controller.update);

    // DELETE
    app.delete('group/:_id', controller.delete);

}
