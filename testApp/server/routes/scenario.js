module.exports = function (app) {
    /*  request routes
     ---------------
     We create a variable "user" that holds the controllers object.
     We map the URL to a method in the created variable "user".
     In this example is a mapping for every CRUD action.
     */
    var controller = require('../app/controllers/scenario.js');

    // CREATE
    app.post('scenario', controller.create);
    // RETRIEVE
    app.get('scenario', controller.list);
    app.get('scenario/:_id', controller.detail);

    // UPDATE
    app.put('scenario/:_id', controller.update);

    // DELETE
    app.delete('scenario/:_id', controller.delete);

    // execute
    app.get('executeScenario/:_id', controller.executeScenario);

}