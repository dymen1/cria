/* global XDomainRequest */
(function () {
    var data = window.data;
    var exchangeDataUrl = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20csv%20where%20url%3D'http%3A%2F%2Fdownload.finance.yahoo.com%2Fd%2Fquotes.csv%3Fs%3Dbcs%2Bstt%2Bjpm%2Blgen.l%2Bubs%2Bdb%2Bben%2Bcs%2Bbk%2Bkn.pa%2Bgs%2Blm%2Bms%2Bmtu%2Bntrs%2Bgle.pa%2Bbac%2Bav%2Bsdr.l%2Bdodgx%2Bslf%2Bsl.l%2Bnmr%2Bing%2Bbnp.pa%26f%3Dsl1d1t1c1ohgv%26e%E2%80%8C%E2%80%8B%3D.csv'&format=json&diagnostics=false&callback=",
        exchangeDataFallbackUrl = "",
        useFallback = false,
        received = true,
        corsTimer;

    window.crud = {

        /** maakt alle nodes (menu, talbe en msgbox waar de rest van de pagina aan wordt gehangen
         *
         * @param crud
         */
        createNodes: function (crud) {
            var bodyNode, tableNode;

            // check if data is defined
            if (data !== undefined) {

                // Get body node
                bodyNode = document.getElementsByTagName("body")[0];

                crud.createMenu(bodyNode);

                // Create a table
                tableNode = document.createElement("table");
                tableNode.id = "stockData";
                tableNode.class = "stockData";
                bodyNode.appendChild(tableNode);

                crud.createTableHeader(tableNode);

                crud.createTableContent(tableNode);

                crud.createMsgBox(bodyNode);

            } else {
                window.alert("no data!");
            }
        },

        /** maakt de msgbox waarin de meldingen komen te staan met
         * betrekking tot het ophalen en genereren van stockdata
         *
         * @param bodyNode
         */
        createMsgBox: function (bodyNode) {
            var msgContainerNode;
            msgContainerNode = document.createElement("div");
            msgContainerNode.id = "msg";
            bodyNode.appendChild(msgContainerNode);
        },

        /** maakt een message (melding) die wordt weergegeven in de msgbox
         *
         * @param msgText
         */
        createMessage: function (msgText) {
            var msgContainerNode, textNode, spanNode, fillNode, newDate = new Date();

            msgContainerNode = document.getElementById("msg");

            textNode = document.createElement("p");
            textNode.innerHTML = newDate.getDate() + "-" +
                (newDate.getMonth() + 1) + "-" +
                newDate.getFullYear() + " " +
                ("0" + newDate.getHours()).slice(-2) + ":" +
                ("0" + newDate.getMinutes()).slice(-2) + ":" +
                ("0" + newDate.getSeconds()).slice(-2) + " "; //get two digit values
            msgContainerNode.insertBefore(textNode, msgContainerNode.children[0]);

            spanNode = document.createElement("span");
            spanNode.title = "makeCorsRequest";
            spanNode.innerHTML = msgText; //"Retrieving live data...";
            msgContainerNode.insertBefore(spanNode, msgContainerNode.children[1]);

            fillNode = document.createElement("br");
            msgContainerNode.insertBefore(fillNode, msgContainerNode.children[2]);
        },

        /** maakt het menu bovenaan de pagina waardoor er een keuze is in welke data gebuirkt wordt
         *
         * @param bodyNode
         */
        createMenu: function (bodyNode) {
            var menuNode, linkNode, fillNode;
            menuNode = document.createElement("div");
            menuNode.id = "header";
            bodyNode.appendChild(menuNode);

            linkNode = document.createElement("a");
            linkNode.href = "?live=true&amp;remote=true&amp;datapoints=10&amp;refresh=4000";
            linkNode.innerHTML = "live (with test fallback)";
            menuNode.appendChild(linkNode);

            fillNode = document.createElement("p");
            fillNode.innerHTML = " | ";
            menuNode.appendChild(fillNode);

            linkNode = document.createElement("a");
            linkNode.href = "?test=true&amp;remote=true&amp;datapoints=10&amp;refresh=4000";
            linkNode.innerHTML = "test url";
            menuNode.appendChild(linkNode);

            fillNode = document.createElement("p");
            fillNode.innerHTML = " | ";
            menuNode.appendChild(fillNode);

            linkNode = document.createElement("a");
            linkNode.href = "?local=true&amp;remote=false&amp;datapoints=10&amp;refresh=4000";
            linkNode.innerHTML = "generate testdata";
            menuNode.appendChild(linkNode);
        },

        /** maakt de header van de tabel waardoor de gebruiker duidelijk
         * kan zien wat er in iedere kolom wordt weergegeven
         *
         * @param tableNode
         */
        createTableHeader: function (tableNode) {
            var tableRowNode, tableCellNode;
            // Create table header
            tableRowNode = document.createElement("tr");
            tableRowNode.id = "tableHeader";
            tableNode.appendChild(tableRowNode);

            tableCellNode = document.createElement("th");
            tableCellNode.innerHTML = "symbol";
            tableRowNode.appendChild(tableCellNode);

            tableCellNode = document.createElement("th");
            tableCellNode.innerHTML = "last trade price only";
            tableRowNode.appendChild(tableCellNode);

            tableCellNode = document.createElement("th");
            tableCellNode.innerHTML = "last trade date";
            tableRowNode.appendChild(tableCellNode);

            tableCellNode = document.createElement("th");
            tableCellNode.innerHTML = "last trade time";
            tableRowNode.appendChild(tableCellNode);

            tableCellNode = document.createElement("th");
            tableCellNode.innerHTML = "change";
            tableRowNode.appendChild(tableCellNode);

            tableCellNode = document.createElement("th");
            tableCellNode.innerHTML = "open";
            tableRowNode.appendChild(tableCellNode);

            tableCellNode = document.createElement("th");
            tableCellNode.innerHTML = "day's high";
            tableRowNode.appendChild(tableCellNode);

            tableCellNode = document.createElement("th");
            tableCellNode.innerHTML = "day's low";
            tableRowNode.appendChild(tableCellNode);

            tableCellNode = document.createElement("th");
            tableCellNode.innerHTML = "volume";
            tableRowNode.appendChild(tableCellNode);
        },

        /** maakt de tabel rijen aan en vult deze met standaard data
         *
         * @param tableNode
         */
        createTableContent: function (tableNode) {
            var tableRowNode, tableCellNode, i, property, cellModel;
            // Create a table row element
            for (i = 0; i < data.query.results.row.length; i++) {
                tableRowNode = document.createElement("tr");
                tableRowNode.id = data.query.results.row[i].col0;

                // Append new node to the table
                tableNode.appendChild(tableRowNode);

                // Create a table cell element
                for (property in data.query.results.row[i]) {
                    tableCellNode = document.createElement("td");

                    //inheritance
                    cellModel = new window.CellModel(data, i);
                    cellModel.setCellData(property);
                    tableCellNode.innerHTML = cellModel.getCellData();

                    // Append new node to the table row
                    tableRowNode.appendChild(tableCellNode);
                }
            }
        },

        retrieveNodes: function () {

            /**
             * Returns an object reference to the identified element.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.getElementById
             */
            document.getElementById("someId");

            /**
             * Returns a list of elements with the given class name.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.getElementsByClassName
             */
            document.getElementsByClassName("someClass");

            /**
             * Returns a list of elements with the given tag name and namespace.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.getElementsByTagName
             */
            document.getElementsByTagName("body");

            /**
             * Returns a list of elements with the given name.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.getElementsByName
             */
            document.getElementsByName("someName");

            /**
             * Returns the first Element node within the document, in document order, that matches the specified selectors.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.querySelector
             */
            document.querySelector("div.someClass p.someOtherClass");

            /**
             * Returns a list of all the Element nodes within the document that match the specified selectors.
             * @see https://developer.mozilla.org/en-US/docs/Web/API/Document.querySelectorAll
             */
            document.querySelectorAll("div.someClass p.someOtherClass");

        },

        /** update de tabel aan de hand van de meegeven data en
         * corrigeert meteen de kolom waarin wordt aangegeven hoeveel de waarde van en aandeel is veranderd
         *
         * @param crud
         * @param updatedData
         */
        updateNodes: function (crud, updatedData) {
            updatedData = crud.correctReceivedData(updatedData);
            var i, j, property, tableRowNode;
            for (i = 0; i < data.query.results.row.length; i++) {
                tableRowNode = document.getElementById(updatedData.query.results.row[i].col0);

                // Set table cell content
                j = 0;
                for (property in data.query.results.row[i]) {
                    tableRowNode.childNodes[j].innerHTML = updatedData.query.results.row[i][property];
                    if (property === "col4" && updatedData.query.results.row[i][property] > 0) {
                        tableRowNode.childNodes[j].classList.add("higher");
                        tableRowNode.childNodes[j].classList.remove("lower");
                    } else if (property === "col4" && updatedData.query.results.row[i][property] < 0) {
                        tableRowNode.childNodes[j].classList.add("lower");
                        tableRowNode.childNodes[j].classList.remove("higher");
                    } else if (property === "col4") {
                        tableRowNode.childNodes[j].classList.remove("higher");
                        tableRowNode.childNodes[j].classList.remove("lower");
                    }
                    if (j === 8) {
                        j = -1;
                    }
                    j++;
                }
            }
        },

        /** corrigeert meegegeven data zodat de kolom die aangeeft hoeveel
         * de waarde van een aandeel is veranderd correct is
         *
         * @param updatedData
         * @returns {*}
         */
        correctReceivedData: function (updatedData) {
            var i, tableRowNode;
            for (i = 0; i < data.query.results.row.length; i++) {
                tableRowNode = document.getElementById(updatedData.query.results.row[i].col0);

                // Adjust table cell content updatedData
                updatedData.query.results.row[i].col4 = (tableRowNode.childNodes[1].innerHTML - updatedData.query.results.row[i].col1).toFixed(2);
                if (updatedData.query.results.row[i].col4 > 0) {
                    updatedData.query.results.row[i].col4 = "+" + updatedData.query.results.row[i].col4;
                }
            }
            return updatedData;
        },

        /** haalt de variabelen die in de url staan op,
         * deze worden namelijk gebruikt in het bepalen welke data moet worden gebruikt
         *
         * @returns {{}}
         */
        getGet: function () {
            var obj = {}, params = location.search.slice(1).split('&amp;'), i, keyVal, len;
            for (i = 0, len = params.length; i < len; i++) {
                keyVal = params[i].split('=');
                obj[decodeURIComponent(keyVal[0])] = decodeURIComponent(keyVal[1]);
            }
            return obj;
        },

        /** maakt een CORSrequest en handeld deze af
         *
         * @param crud
         */
        createCORSRequest: function (crud) {
            if (received) {
                crud.createMessage("Retrieving live data...");
                var xhr = crud.createXMLHttpRequest(crud, 'GET', (!useFallback) ? exchangeDataUrl : exchangeDataFallbackUrl);
                xhr.addEventListener('load', function () {
                    var responseText = JSON.parse(xhr.responseText);
                    if (responseText.query.results.row[0].col0 === "BCS") {
                        crud.updateNodes(crud, responseText);
                    }
                });
            } else {
                received = false;
            }
        },

        /** maakt een XMLHttprequest en handeld deze af
         *
         * @param crud
         * @param method
         * @param url
         * @returns {XMLHttpRequest}
         */
        createXMLHttpRequest: function (crud, method, url) {
            var request;
            request = new window.XMLHttpRequest();

            if (request.withCredentials !== undefined) {
                request.open(method, url, true);
            } else if (typeof XDomainRequest) {
                request = new XDomainRequest();
                request.open(method, url);
            } else {
                request = null;
            }

            request.onreadystatechange = state_change();
            function state_change() {
                if (request.readyState === 4) {// 4 = "loaded"
                    if (request.status !== 200) {
                        crud.createMessage("Problem retrieving live data");
                    } else if (request.status === 200) {
                        crud.createMessage("Retrieved live data!");
                    }
                    received = true;
                }
            }

            request.send(null);
            return request;
        },

        /** zorgt ervoor dat de stock quotes simuleert alsof er
         * echte data wordt opgehaald zodat er getest kan worden
         *
         * @param crud
         */
        useTestData: function (crud) {
            crud.createMessage("Retrieved test data!");
            var testData = data, i;
            for (i = 0; i < data.query.results.row.length; i++) {
                // Adjust column1 content
                testData.query.results.row[i].col1 = (testData.query.results.row[i].col1 * (Math.random() * (1.1 - 0.9 + 1)) + 0.9).toFixed(2);
                testData.query.results.row[i].col8 = Math.floor((Math.random() * (100000 - 100 + 1)) + 100);
            }
            testData = crud.correctReceivedData(testData);
            crud.updateNodes(crud, testData);
        },

        /**
         *  zorgt ervoor dat alle functies die nodig zijn om stock quotes te kunnen gebruiken worden aangeroepen
         */
        init: function (crud) {
            var dataGet = crud.getGet();
            crud.createNodes(crud);
            crud.retrieveNodes();

            var refreshRate;
            if (dataGet.refresh !== undefined) {
                refreshRate = dataGet.refresh;
            } else {
                refreshRate = 4000;
            }

            if (dataGet.remote === undefined || dataGet.remote === "false") {
                corsTimer = setInterval(function () {
                    crud.useTestData(crud);
                }, refreshRate);
            } else {
                corsTimer = setInterval(function () {
                    crud.createCORSRequest(crud);
                }, refreshRate);
            }
        }
    }
    ;
}
    ()
    )
;

/**
 * zodra de pagina geladen is roept deze de init aan tenzij er unit tests worden uitgevoerd
 * dit is herkenbaar omdat deze in de map test zitten, dit controleer ik met behulp van de URL
 */
window.onload = function () {
    if (window.location.href.indexOf("test") <= -1) {
        window.crud.init(window.crud);
    }
};