function CellModel(data, i) {
    this.innerHTML = undefined;
    this.prototype = new window.StockModel(data, i);
}

CellModel.setCellData = function (property) {
    this.innerHTML = this.prototype.getStockData(property);
};

CellModel.getCellData = function () {
    return this.innerHTML;
};
