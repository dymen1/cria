/**
 * Created by D on 3-2-14.
 */
javascript: (function () {
    if ($("#allin").length === 0) {
        var doto = window.location.host.indexOf("dota") > 0;
        var firstTeam = doto ? "radiant" : "blue";
        var secondTeam = doto ? "dire" : "red";
        var randomTeam = "YOLObet (random)";
        var sideBets = "sideBets (random)";
        var autoBet = "autoBet";
        var autoBetCancel = "Cancel autoBet";

        $("#user_dash").append("" +
            "<div class='dash_element' id='allin' style='...'>" +
                "<p style='margin-top:0px;'>&nbsp;&nbsp;&nbsp;YOLObet buttons&nbsp;&nbsp;&nbsp;<br />" +
                    "<input id='use_insurance' type='checkbox'>" +
                    "<label for='use_insurance'>use insurance</ label><br />" +
                    "<input class='popup_button' id='allin1' type='button' value='" + firstTeam + "'>" +
                    "<input class='popup_button' id='allin2' type='button' value='" + secondTeam + "'>" +
                    "<input class='popup_button' id='allinRandom' type='button' value='" + randomTeam + "'><br />" +
                    "<input class='popup_button' id='sideBet' type='button' value='" + sideBets + "'>" +
                    "<input class='popup_button' id='allinAuto' type='button' value='" + autoBet + "'>" +
                    "<input class='popup_button' id='shutdown_auto' type='button' value='" + autoBetCancel + "'>" +
                    "<input class='popup_button' id='shutdown_allin' type='button' value='Close'><br />" +
                "</p >" +
            "</ div>");

        function placeBetOne() {
            if (($('#main_bets_dash').is(':visible')) && !($("#div_main_radiant").is(":visible") || $("#div_main_dire").is(":visible"))) {
                var doto = window.location.host.indexOf("dota") > 0;
                var firstTeam = doto ? "radiant" : "blue";
                var secondTeam = doto ? "dire" : "red";
                var side = firstTeam;
                var salt = parseInt($("#wallet_payload").text());
                $.ajax({                    type: "PUT", dataType: "json", data: "[]", url: "/place_bet/main/" + salt + "/" + side, success: function (bet_event_payload) {
                    if ($("#use_insurance").is(":checked")) {
                        $.ajax({                                type: "PUT", dataType: "json", data: "[]", url: "/coldfeet/", success: function (coldfeet_event_payload) {
                            $.event.trigger({                                        type: "requestUpdate", payload: coldfeet_event_payload                                    });
                        }                            });
                    }
                }                });
                $('#main_bets_dash').hide();
                buy_sound.play();
            }
        }

        function placeBetTwo() {
            if (($('#main_bets_dash').is(':visible')) && !($("#div_main_radiant").is(":visible") || $("#div_main_dire").is(":visible"))) {
                var doto = window.location.host.indexOf("dota") > 0;
                var firstTeam = doto ? "radiant" : "blue";
                var secondTeam = doto ? "dire" : "red";
                var side = secondTeam;
                var salt = parseInt($("#wallet_payload").text());
                $.ajax({                    type: "PUT", dataType: "json", data: "[]", url: "/place_bet/main/" + salt + "/" + side, success: function (bet_event_payload) {
                    if ($("#use_insurance").is(":checked")) {
                        $.ajax({                                type: "PUT", dataType: "json", data: "[]", url: "/coldfeet/", success: function (coldfeet_event_payload) {
                            $.event.trigger({                                        type: "requestUpdate", payload: coldfeet_event_payload                                    });
                        }                            });
                    }
                }                });
                $('#main_bets_dash').hide();
                buy_sound.play();
            }
        }

        function placeBetRandom() {
            if (($('#main_bets_dash').is(':visible')) && !($("#div_main_radiant").is(":visible") || $("#div_main_dire").is(":visible"))) {
                var random = Math.ceil(Math.random() * 2) == 1;
                var doto = window.location.host.indexOf("dota") > 0;
                var firstTeam = doto ? "radiant" : "blue";
                var secondTeam = doto ? "dire" : "red";
                var randomTeam = random ? firstTeam : secondTeam;
                var side = randomTeam;
                var salt = parseInt($("#wallet_payload").text());
                $.ajax({                    type: "PUT", dataType: "json", data: "[]", url: "/place_bet/main/" + salt + "/" + side, success: function (bet_event_payload) {
                    if ($("#use_insurance").is(":checked")) {
                        $.ajax({                                type: "PUT", dataType: "json", data: "[]", url: "/coldfeet/", success: function (coldfeet_event_payload) {
                            $.event.trigger({                                        type: "requestUpdate", payload: coldfeet_event_payload                                    });
                        }                            });
                    }
                }                });
                $('#main_bets_dash').hide();
                buy_sound.play();
            }
        }

        function placeSideBet() {
            if ($('#bet_taker').is(':visible')) {
                var random = Math.ceil(Math.random() * 2) == 1;
                var doto = window.location.host.indexOf("dota") > 0;
                var firstTeam = doto ? "radiant" : "blue";
                var secondTeam = doto ? "dire" : "red";
                var randomTeam = random ? firstTeam : secondTeam;
                var side = randomTeam;
                var salt = 100;
                $.ajax({                    type: "PUT", dataType: "json", data: "[]", url: "/place_bet/team_death/" + salt + "/" + side, success: function (bet_event_payload) {
                    if ($("#use_insurance").is(":checked")) {
                        $.ajax({                                type: "PUT", dataType: "json", data: "[]", url: "/coldfeet/", success: function (coldfeet_event_payload) {
                            $.event.trigger({                                        type: "requestUpdate", payload: coldfeet_event_payload                                    });
                        }                            });
                    }
                }                });
                $('#main_bets_dash').hide();
                buy_sound.play();
            }
        }

        function placeSideBet() {
            $.autoSideBetTimer = setInterval(function () {
                placeSideBet();
            }, 44000);
        }

        function auto_bet_start() {
            $.autoBetTimer = setInterval(function () {
                placeBetRandom();
            }, 120000);
        }

        function auto_bet_shutdown() {
            window.clearInterval($.autoBetTimer);
            $.autoBetTimer = undefined;
        }

        function ai_shutdown() {
            window.clearInterval($.autoBetTimer);
            $.autoBetTimer = undefined;
            window.clearInterval($.autoSideBetTimer);
            $.autoSideBetTimer = undefined;
            window.clearInterval($.ai_interval);
            $("#allin").remove();
            $.ai_interval = undefined;
        }

        $("#allin1").click(placeBetOne);
        $("#allin2").click(placeBetTwo);
        $("#allinRandom").click(placeBetRandom);
        $("#sideBet").click(placeSideBet);
        $("#allinAuto").click(auto_bet_start);
        $("#shutdown_auto").click(auto_bet_shutdown);
        $("#shutdown_allin").click(ai_shutdown);
    }
})();