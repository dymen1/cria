Stockquotes
===========

Edo Spijker

507901

-------
Design
-------
- init
    - calls GetURL
    - checks if the table is rendered, else there is a loading text on the top of the site.
- renderdata
    - renders the given data on the screen, this can be realtime information from "http://cria.tezzt.nl/examples/stockquotes/index.php?cbfunc=&_=1394530444871" (with itself is generated data)
        or fallback data generated within.
    - it checks if there is already a table on the screen, if so the first one is deleted and updated with the new one.
    - it calls the function fillRow to fill each row with the given object, which is one of the 27 companies that the GetURL gets
- fillRow
    - it fills each row with the given object, which is one of the 27 companies that the GetURL gets. and creates a classname for each row (minus plus normal)  according to the change
-createCORSRequest
    - this function was given by the professor and gets an object from the given URL with the given method (POST, GET)
- randomNumber
    - this function creates a random number between 5% up and 5% down from the given number. and returns this.
- makeRandom
    - if there cant be made a connection with the url in GetURL, then the program switches from realtime to fallback, where a copy of the JSON object gets altered to look like real data.
    - it loops through each company and modifies the 2th column and 5th column.
-------
Concepts
---------
* Objects
    Fallback is an JSON object
    JSONobject is an JSON object
    stockQuotes itself is an object
    The XMLHttpRequest is an object
* JSON
    i used JSON for XMLHttpRequest, the incomming data is a JSON object
* CORS Request
I used a CORS request to make a table to view the current stocks for 27 different companies

--------
Objects
--------
in the code below i parse a string to an JSON object.
   ```
        stockQuotes.JSONObject = JSON.parse(xhr.responseText);
   ```

--------
JSON
--------
in the code below i parse a string to an JSON object.
   ```
        stockQuotes.JSONObject = JSON.parse(xhr.responseText);
   ```
i can later use this JSON object for multiple uses (like get the last stock data);

------------
CORS Request
------------
this function was given to us by our professor.
With this method my program created a CORS request. It checks if the current browser can create a XMLHTTPRequest, if thats not the case, it creates a XDomainRequest.
        ```
            createCORSRequest: function (method, url) {
                var xhr;
                xhr = new window.XMLHttpRequest();
                if (xhr.withCredentials !== undefined) {
                    //XHR for chrome/FF/opera/safari
                    xhr.open(method, url, true);
                } else if (typeof XDomainRequest) {
                    //xdomainrequest for IE
                    xhr = new XDomainRequest();
                    xhr.open(method, url);
                } else {
                    //not supported
                    xhr = null;
                }
            return xhr;
        ```

