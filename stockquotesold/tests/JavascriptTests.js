describe("Testing", function () {
    it("test randomNumber", function () {
        expect(stockQuotes.randomNumber(10000)).not.toEqual(stockQuotes.randomNumber(10000));
    });

    it("test make random", function () {
        var fallback = clone(stockQuotes.fallbackData);
        stockQuotes.makeRandom();
        expect(fallback.query.results.row[0].col1).not.toEqual(stockQuotes.fallbackData.query.results.row[0].col1);
    });

    it("test random data", function () {
        expect(document.getElementsByClassName('table')[0]).not.toEqual(null);

    });

    it("test init", function () {
        expect(document.getElementsByName('loading')).not.toEqual(null);
       // expect(stockQuotes.loading).toEqual(null);

    });

    /**
     * safe copy by value, not by reference
     * not my responsibility
     * @param obj
     * @returns {Object}
     */
    function clone(obj){
        if(obj == null || typeof(obj) != 'object')
            return obj;

        var temp = new obj.constructor();
        for(var key in obj)
            temp[key] = clone(obj[key]);

        return temp;
    }
});
