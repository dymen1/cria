/*jslint browser:true, devel:true, forin: true*/
/*global stockQuotes:false, timeOut: true, XDomainRequest: false, reserveData: false*/

(function () {
    "use strict";
    var stockQuotes = {

        /*
         this is the object that holds the realtime stockquotes
         */
        JSONObject: null,
        /*
         this is the object that holds the fallbackData stockquotes in case there is no internet
         */
        fallbackData: reserveData,
        /*
         this is a boolean for the internet connection
         */
        internet: undefined,
        /*
         this is a variable that checks if the table is loaded.
         */
        loading: undefined,

        fallback: undefined,
        /**
         * this function set everything in motion
         * and notifies the user that the system is loading
         */
        init: function () {
            var bodyNode;
            console.log(stockQuotes.loading);
            if (stockQuotes.loading === undefined && stockQuotes.fallback === undefined) {
                stockQuotes.loading = document.createTextNode("loading");
                stockQuotes.loading.id = "loading";
                bodyNode = document.getElementsByTagName("body")[0];
                bodyNode.appendChild(stockQuotes.loading);
            } else if ((stockQuotes.internet === true || stockQuotes.fallback !== undefined) && stockQuotes.loading !== null) {
                stockQuotes.loading.parentNode.removeChild(stockQuotes.loading);
                stockQuotes.loading = null;
            }
            stockQuotes.getURL();
        },

        /**
         *  this function renders the data from a given JSON object to the screen in a table.
         *
         * @param object is the JSON object (fallbackData or realtime)
         */
        renderData: function (object) {
            var bodyNode, table, tBody, q, i, attributeName, attributeValue, child, row;

            bodyNode = document.getElementsByTagName("body")[0];

            if (document.getElementsByClassName('table')[0] !== undefined) {
                child = document.getElementsByClassName('table')[0];
                child.parentNode.removeChild(child);
            }
            bodyNode = document.getElementsByTagName("body")[0];
            table = document.createElement('table');
            table.className = "table";
            tBody = document.createElement('tBody');

            for (i = 0; i < object.query.results.row.length; i = i + 1) {
                q = object.query.results.row[i];

                row = document.createElement("tr");

                /**
                 * for one reason jslint gives an error on this for in, i googled it and it should be good
                 */

                for (attributeName in q) {
                    if (q.hasOwnProperty(attributeName)) {
                        attributeValue = q[attributeName];
                        stockQuotes.fillRow(attributeName, row, attributeValue);
                    }
                    tBody.appendChild(row);
                }

                table.appendChild(tBody);

                bodyNode.appendChild(table);
            }
        },

        /**
         * this function fills the specific rows with the given attributevalues
         *
         * @param attributeName the name of the attribute
         * @param q the specific company
         * @param row the row where the values have to be added
         * @param attributeValue the value of the attribute
         */
        fillRow: function (attributeName, row, attributeValue) {
            var cell, cellText;
            cell = document.createElement("td");
            cellText = document.createTextNode(attributeValue);
            cell.appendChild(cellText);
            if (attributeName === "col4" && attributeValue > 0) {
                row.className = "plus";
            } else if (attributeName === "col4" && attributeValue < 0) {
                row.className = "minus";
            } else if (attributeName === "col4" && attributeValue === 0) {
                row.className = "normal";
            }
            row.appendChild(cell);
        },

        /**
         * this functions tries to connect to a given URL
         *
         * @param method post or get
         * @param url the url
         * @returns {XMLHttpRequest} a xhr object
         */
        createCORSRequest: function (method, url) {
            var xhr;
            xhr = new window.XMLHttpRequest();

            if (xhr.withCredentials !== undefined) {
                //XHR for chrome/FF/opera/safari
                xhr.open(method, url, true);
            } else if (typeof XDomainRequest) {
                //xdomainrequest for IE
                xhr = new XDomainRequest();
                xhr.open(method, url);
            } else {
                //not supported
                xhr = null;
            }
            return xhr;

        },

        /**
         * this function generates a random number with the given value
         * @param attributeValue float
         * @returns {number}
         */
        randomNumber: function (attributeValue) {
            var min, max;
            min = attributeValue * 0.95;
            max = attributeValue * 1.05;
            parseFloat(attributeValue);

            return parseFloat((Math.random() * (max - min + 1) + min));
        },

        /**
         * this function makes random fallbackData data en fills this in the fallbackData object
         */
        makeRandom: function () {
            var i, q, attributeName, attributeValue, change;
            for (i = 0; i < stockQuotes.fallbackData.query.results.row.length; i = i + 1) {
                q = stockQuotes.fallbackData.query.results.row[i];


                for (attributeName in q) {
                    if (q.hasOwnProperty(attributeName)) {
                        attributeValue = q[attributeName];
                        if (attributeName === "col1") {
                            change = (stockQuotes.randomNumber(attributeValue).toFixed(2) - attributeValue).toFixed(2);
                            q.col1 =  (parseFloat(attributeValue) + parseFloat(change)).toFixed(2);
                            q.col4 = change;
                        }
                    }
                }
            }
        },

        /**
         * this function calls the createCORSRequest function and uses this to create a JSON object.
         * it  also checks if there is an internet connection.
         */
        getURL: function () {
            var xhr, xhrTimeout, bodyNode;
            xhr = stockQuotes.createCORSRequest("get", "http://cria.tezzt.nl/examples/stockquotes/index.php?cbfunc=&_=1394530444871");

            /**
             * this function is called if there is an timeOut
             */
            function timeOut1() {
                stockQuotes.internet = false;
            }

            /**
             * this is called when xhr is ready
             */
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    stockQuotes.internet = true;
                    stockQuotes.JSONObject = JSON.parse(xhr.responseText);
                    if (stockQuotes.JSONObject !== null || stockQuotes.JSONObject !== undefined) {
                        stockQuotes.fallbackData = stockQuotes.clone(stockQuotes.JSONObject);
                    }
                    stockQuotes.renderData(stockQuotes.JSONObject);
                    clearTimeout(xhrTimeout);
                }
            };

            xhrTimeout = setTimeout(timeOut1, 2000);
            if (stockQuotes.internet !== false) {
                xhr.send();
            }

            if (stockQuotes.internet === false) {
                stockQuotes.makeRandom();
                stockQuotes.renderData(stockQuotes.fallbackData);
                if (stockQuotes.fallback === undefined) {
                    stockQuotes.fallback = document.createTextNode("fallbackData active");
                    stockQuotes.fallback.id = "fallback";
                    bodyNode = document.getElementsByTagName("body")[0];
                    bodyNode.appendChild(stockQuotes.fallback);
                }
            }
        },

        /**
         * safe copy by value, not by reference
         * not my responsibility
         * @param obj
         * @returns {Object}
         */
        clone: function (obj) {
            var temp, key;
            if (obj === null || (typeof obj !== 'object')) {
                return obj;
            }

            temp = new obj.constructor();
            for (key in obj) {
                temp[key] = stockQuotes.clone(obj[key]);
            }
            return temp;
        }
    };

    window.stockQuotes = stockQuotes;
}());
/**
 * this interval kickstarts this program every 1000 ms
 */
window.setInterval(stockQuotes.init, 1000);